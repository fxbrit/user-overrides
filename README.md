# user-overrides

Overrides for [arkenfox's user.js](https://github.com/arkenfox/user.js/) for Firefox.

Thanks to [arkenfox](https://github.com/arkenfox), if you don't know about user.js project check it out.
